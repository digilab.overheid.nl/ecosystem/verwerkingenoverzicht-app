# Verwerkingenoverzicht app

Small project to test the performance of requests from mobile phones to a large number of online sources.

## Background
Several projects for the Dutch government allow citizens to request data from multiple government services. Examples of such projects are [Vorderingenoverzicht Rijk](https://vorijk.nl/) and [Verwerkingenlogging](https://www.gemmaonline.nl/index.php/GEMMA2/0.9/id-c158d9fc-6421-4d8a-8b2e-eaa33f43ecf8).

Government data is often stored decentrally. In practice, data of the citizen can be stored in hundreds of sources, some of which can be temporarily down, slow, or have poor network connections. To simulate this situation, we use [toxiyproxy](https://github.com/Shopify/toxiproxy).

This app uses Flutter and connects to the [backend service](https://gitlab.com/digilab.overheid.nl/ecosystem/verwerkingenoverzicht) for this project.


## Findings

Flutter/Dart has several mechanisms for background jobs:
- [`isolate`](https://docs.flutter.dev/perf/isolates)
  - Isolates are similar to threads, but differ in that isolates have their own isolated memory.
  - When running tasks in an isolate, the UI is not slowed down and the FPS are steady.
  - Information can be exchanged with the main isolate using a [ReceivePort](https://api.dart.dev/stable/dart-isolate/ReceivePort-class.html) and/or [SendPort](https://api.dart.dev/stable/dart-isolate/SendPort-class.html).
  - Isolates only work on mobile, not on web. For web, alternatives such as service workers can be used.
- [`compute`](https://api.flutter.dev/flutter/foundation/compute.html)
  - `compute` can also be used on the Flutter web platform. The `compute()` method runs the computation on the main thread on the web, but spawns a new thread on mobile devices.
  - On mobile and desktop platforms, `await compute(foo, bar)` is equivalent to `await Isolate.run(() => foo(bar))`.
  - `compute` is more limited  in the sense that it is difficult to stop/cancel during computation and makes it harder to share information with the main thread/isolate, except when completely finished.
- [Android WorkManager](https://developer.android.com/topic/libraries/architecture/workmanager)
  - Android WorkManager can be used to execute immediate tasks, deferred tasks, which can be long running and/or periodic tasks.
  - The advantage of this is that tasks can run in the background even when the app is closed.
- iOS: [BGProcessingTask](https://developer.apple.com/documentation/backgroundtasks/bgprocessingtask) and [BGAppRefreshTask](https://developer.apple.com/documentation/backgroundtasks/bgapprefreshtask)
  - iOS supports `BGAppRefreshTask` for typically short tasks and `BGProcessingTask` for long tasks, both of which can be run in the background.
  - Such tasks have some limitations and may be stopped after a certain amount of time. See [this](https://developer.apple.com/forums/thread/707503) and [this](https://developer.apple.com/forums/thread/685525) thread for more information.
- Wrapper libraries are available for Flutter to write code once for both Android WorkManager and iOS BGProcessingTask, such as [workmanager](https://pub.dev/packages/workmanager) and [flutter_background_service](https://pub.dev/packages/flutter_background_service).
  - Both of these libraries have relatively many open issues on GitHub and seem not (always) to work with Android simulator.
- Concurrency: [pool](https://pub.dev/packages/pool) library
  - When doing concurrent tasks (e.g. HTTP requests in an isolate), the `pool` library, developed by Flutter team, can be used. This is similar to using a [semaphore](https://pkg.go.dev/golang.org/x/sync/semaphore) in Go.

## Results
- For this project, in order to test performance, multiple concurrent requests are executed using a `pool` in an `isolate`.
- This works fine on mobile.
- The Android emulator might freeze after a few hundred requests, apparently due to issues with its internal proxy.
- Around 50 simultaneous requests seems right when requesting data from 1,000 resources.
- Further improvements are possible in the user interface and/or experimentation with Android WorkManager and iOS BGProcessingTask.
