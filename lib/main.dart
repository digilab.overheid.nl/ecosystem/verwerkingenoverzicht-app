import 'dart:async';
import 'dart:io';
import 'package:flutter/material.dart';
import 'dart:isolate';
import 'dart:convert';
import 'package:pool/pool.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatefulWidget {
  const MainApp({super.key});

  @override
  State<MainApp> createState() => _MainAppState();
}

class SourceResponse {
  String source;
  int responseCode;

  // Constructor
  SourceResponse(this.source, this.responseCode);
}

class WorkerStatus {
  String status;
  int? numberOfSources;

  // Constructor
  WorkerStatus(this.status, [this.numberOfSources]);
}

class _MainAppState extends State<MainApp> {
  Isolate? _isolate;
  bool _isolateIsRunning = false;

  // Define two maps: one for the status per source and one with the number of responses per status
  Map<String, int> sourceStatuses = {};
  Map<int, int> responsesPerStatus = {};
  int numberOfSources = 0;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      body: Center(
        child: Column(children: [
          const SizedBox(height: 60),
          Text('Currently running: $_isolateIsRunning'),
          const SizedBox(height: 30),
          ElevatedButton(
            onPressed: () async {
              print('start button pressed');

              if (!_isolateIsRunning) {
                setState(() {
                  _isolateIsRunning = true;

                  // Reset the maps
                  sourceStatuses = {};
                  responsesPerStatus = {};
                });

                // Create a ReceivePort to listen for messages from the background isolate
                ReceivePort receivePort = ReceivePort();

                _isolate = await Isolate.spawn<SendPort>(
                  entryPoint, // Function
                  receivePort.sendPort, // Input/payload
                  errorsAreFatal:
                      true, // uncaught errors will terminate the isolate
                  debugName: 'Request job', // name in debuggers and logging
                );

                // Listen for messages from the background task
                receivePort.listen((message) {
                  if (message is WorkerStatus) {
                    print('worker status has changed to: ${message.status}');

                    if (message.status == 'requesting') {
                      setState(() {
                        numberOfSources = message.numberOfSources ?? 0;
                        responsesPerStatus[0] =
                            0; // Note: key 0 is used to store finished sources
                      });
                    }

                    if (message.status == 'finished') {
                      setState(() {
                        _isolateIsRunning = false;
                      });
                    }
                  } else if (message is SourceResponse) {
                    sourceStatuses[message.source] = message
                        .responseCode; // Note: without setState, since the individual source statuses are currently not shown in the widget. IMPROVE: make sure the maps are safe to be used for concurrent writes (e.g using a lock), also below

                    setState(() {
                      responsesPerStatus[message.responseCode] =
                          (responsesPerStatus[message.responseCode] ?? 0) + 1;

                      // Calculate the number of responses per status 0 (= finished) as the remainder of the other statuses. Note: this is possible since the status will normally only be assigned once
                      int numberFinished = 0;
                      responsesPerStatus.forEach((key, value) {
                        if (key != 0) {
                          numberFinished += value;
                        }
                      });

                      responsesPerStatus[0] = numberFinished;

                      // Sort the responsesPerStatus map
                      List<MapEntry<int, int>> sortedEntries =
                          responsesPerStatus.entries.toList();
                      sortedEntries.sort((a, b) => a.key.compareTo(b.key));
                      responsesPerStatus = Map.fromEntries(sortedEntries);
                    });
                  } else {
                    print('received message of unhandled type: $message');
                  }

                  if (message is WorkerStatus && message.status == 'finished') {
                    setState(() {
                      _isolateIsRunning = false;
                    });
                  }
                });
              }
            },
            child: const Text('Start'),
          ),
          ElevatedButton(
            onPressed: () async {
              print('stop button pressed');

              if (_isolateIsRunning) {
                _isolate?.kill(); // Kill the isolate if not null

                setState(() {
                  _isolateIsRunning = false;
                });
                print('worker is stopped');
              } else {
                print('worker was not running');
              }
            },
            child: const Text('Stop'),
          ),
          const SizedBox(height: 30),
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: TweenAnimationBuilder<double>(
              duration: const Duration(milliseconds: 250),
              curve: Curves.easeInOut,
              tween: Tween<double>(
                begin: 0,
                end: (numberOfSources == 0
                    ? 0
                    : (responsesPerStatus[0] ?? 0) / numberOfSources),
              ),
              builder: (context, value, _) =>
                  LinearProgressIndicator(value: value),
            ),
          ),
          const SizedBox(height: 30),
          const Text('Aantal responses per HTTP status code:'),
          SizedBox(
              height: 400, // IMPROVE: use constraints instead?
              child: ListView.builder(
                itemCount: responsesPerStatus.length,
                itemBuilder: (context, index) {
                  int key = responsesPerStatus.keys.elementAt(index);
                  int value = responsesPerStatus[key] ?? 0;

                  return ListTile(
                    title: Text(key == 0
                        ? 'Finished: $value / $numberOfSources'
                        : 'Code $key: $value'),
                  );
                },
              ))
        ]),
      ),
    ));
  }
}

class Source {
  final String endpoint;

  const Source({
    required this.endpoint,
  });

  factory Source.fromJson(Map<String, dynamic> json) {
    return switch (json) {
      {
        'endpoint': String endpoint,
      } =>
        Source(
          endpoint: endpoint,
        ),
      _ => throw const FormatException('Failed to load album.'),
    };
  }
}

// The entry point of the our isolate
Future<void> entryPoint(SendPort sendPort) async {
  sendPort.send(WorkerStatus('started'));

  HttpClient httpClient = HttpClient()
    ..badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);

  httpClient.connectionTimeout = const Duration(
      seconds:
          19); // Note: intentionally lower than the pool timeout. Also below
  httpClient.idleTimeout = const Duration(seconds: 19);
  httpClient.maxConnectionsPerHost = 50; // Note: can probably be omitted

  // Fetch the list of sources
  final request = await httpClient.getUrl(Uri.parse(
      // 'https://verwerkingenoverzicht-discovery-127.0.0.1.nip.io:8443/sources'
      'https://verwerkingenoverzicht-discovery.apps.digilab.network/sources'));

  // Get the response
  HttpClientResponse response = await request.close();

  String responseBody = await response.transform(utf8.decoder).join();

  print('discovery response status: ${response.statusCode}');

  if (response.statusCode != 200) {
    throw Exception('Failed to load sources directory');
  }

  final sourceList = jsonDecode(responseBody) as List;
  List<Source> sources =
      sourceList.map((item) => Source.fromJson(item)).toList();

  sendPort.send(WorkerStatus('requesting', sources.length));

  // Create a Pool that will only allocate multiple resources at once. After the specified duration of inactivity, the pool will throw an error
  final pool = Pool(50, timeout: const Duration(seconds: 20));

  // Send a request to each source. Note: can also be done using the following, but forEach approach seems preferred due to lazy loading optimization, see https://github.com/dart-lang/pool/issues/22
  for (Source source in sources) {
    pool.withResource(() async {
      int statusCode = 0;
      try {
        // Send a request
        final request = await httpClient.getUrl(Uri.parse(source.endpoint));

        // Get the response
        final HttpClientResponse response = await request.close();
        statusCode = response.statusCode;

        await response
            .drain(); // Drain the response, in order to avoid using unnecessary memory, see https://github.com/dart-lang/sdk/issues/41826
      } on SocketException catch (e) {
        // Note: when timing out, a HttpClient will not throw a TimeoutException, but a SocketException
        statusCode = 408;
        print('SocketException: $e');
      } on HandshakeException catch (e) {
        statusCode = 525;
        print('HandshakeException: $e');
      } on HttpException catch (e) {
        statusCode = 500;
        print('HttpException: $e');
      } catch (e) {
        statusCode = 418; // TODO: proper error code for misc errors
        print('Error: $e');
      }

      sendPort.send(SourceResponse(source.endpoint, statusCode));

      // // Sleep for some time
      // await Future.delayed(const Duration(milliseconds: 1000));
    });
  }
  await pool.close();

  // Note: alternative way:
  // Stream stream = pool.forEach(sources, (source) async {
  //   // ...
  // });
  // stream.listen((event) {});
  // await pool.close();

  // Close the HttpClient
  httpClient.close();

  sendPort.send(WorkerStatus('finished'));
}
